﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject Player;
    public TimerUI Timer;
    public EnergyBar EnergyBar;
    public Canvas Canvas;
    public GameObject endObject;

    public float BaseEnergy;
    public float LevelDuration;

    private PlayerControl playerControl;
    public Image youdied;
    public Image youWin;

    public float maxBuffDuration = 2;
    private float buffDuration;
    private bool isBuffActive = false;

    public float speedBuffRange = 1;
    public float remainingEnergyModifier = 1;

    private float energy;





    void Start()
    {
        GameManager.Instance = this;
        playerControl = Player.GetComponent<PlayerControl>();

        youdied = Canvas.GetComponentInChildren<Image>();
        youdied.enabled = false;
        youWin.enabled = false;

        buffDuration = maxBuffDuration;

        energy = BaseEnergy;
        

    }

    void Update()
    {
        if(isBuffActive)
        {
            buffDuration -= Time.deltaTime;
            if (buffDuration <= 0)
            {
                speedBuffRange = 1;
                isBuffActive = false;
                buffDuration = maxBuffDuration;
            }
        }
        playerControl.speedMultiplier = remainingEnergyModifier * speedBuffRange;

        energy -= 1 / LevelDuration * Time.deltaTime;
        EnergyBar.DisplayEnergy(energy / BaseEnergy);
        Timer.DisplayTime(energy * LevelDuration);

        EnergyBar.Image.color = Color.Lerp(Color.red, Color.yellow, energy);

        if (energy <  0.25f * BaseEnergy)
            remainingEnergyModifier = 2.00f;
        else if (energy < 0.5f * BaseEnergy)
            remainingEnergyModifier = 1.66f;
        else if (energy < 0.75f * BaseEnergy)
            remainingEnergyModifier = 1.33f;
        else
            remainingEnergyModifier = 1.00f;

        if (energy <= 0)
        {
            energy = 0;
            UnityEngine.SceneManagement.SceneManager.LoadScene("YouDied");
        }
    }
    /*Methode pour géré le buff de speed ainsi que l'augmentatiuon du temps restant dlors d'un kill.*/
    public void onKill()
    {
        playerControl.AddDash();

        energy += 3 / LevelDuration;
        energy = Mathf.Clamp(energy, 0, 1);

        if(!isBuffActive)
        {
            speedBuffRange = 1.5f;
        }
        else
        {
            buffDuration = maxBuffDuration;
        }
        isBuffActive = true;
    }

    //S'active quand un projectile enemy touche le joueur
    public void onHit()
    {
        remainingEnergyModifier = 1;
        speedBuffRange = 1;
        energy -= 1 / LevelDuration;
    }

    public void Dashing()
    {
        energy -= 5 / LevelDuration; 
    }

    public void onEnd()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Win");
    }
    public void inAcid()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("YouDied");
    }
}



