﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingOutline : MonoBehaviour
{
    public Material buildingMaterial;
    public Material borderMaterial;
    public float borderWidth;
    private GameObject[] borders;

    // Start is called before the first frame update
    void Awake()
    {
        if (buildingMaterial)
            GetComponent<MeshRenderer>().material = buildingMaterial;

        borders = new GameObject[12];

        for (var i = 0; i < 12; i++)
        {
            borders[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            borders[i].transform.SetParent(transform);
            borders[i].GetComponent<MeshRenderer>().material = borderMaterial;
        }

        // top-side edges
        borders[0].transform.localPosition = new Vector3(0.5f, 0.5f, 0);
        borders[1].transform.localPosition = new Vector3(0, 0.5f, 0.5f);
        borders[2].transform.localPosition = new Vector3(-0.5f, 0.5f, 0);
        borders[3].transform.localPosition = new Vector3(0, 0.5f, -0.5f);

        // bottom-side edges
        borders[4].transform.localPosition = new Vector3(0.5f, -0.5f, 0);
        borders[5].transform.localPosition = new Vector3(0, -0.5f, 0.5f);
        borders[6].transform.localPosition = new Vector3(-0.5f, -0.5f, 0);
        borders[7].transform.localPosition = new Vector3(0, -0.5f, -0.5f);

        // middle edges
        borders[8].transform.localPosition = new Vector3(0.5f, 0, 0.5f);
        borders[9].transform.localPosition = new Vector3(-0.5f, 0, 0.5f);
        borders[10].transform.localPosition = new Vector3(-0.5f, 0, -0.5f);
        borders[11].transform.localPosition = new Vector3(0.5f, 0, -0.5f);

        UpdateSize();
    }

    private void Update()
    {
        if (!transform.hasChanged) return;
        UpdateSize();
        transform.hasChanged = false;
    }

    private void UpdateSize()
    {
        borders[0].transform.SetParent(null);
        borders[1].transform.SetParent(null);
        borders[8].transform.SetParent(null);

        borders[0].transform.localScale = new Vector3(1, 1, 1) * borderWidth;
        borders[1].transform.localScale = new Vector3(1, 1, 1) * borderWidth;
        borders[8].transform.localScale = new Vector3(1, 1, 1) * borderWidth;

        borders[0].transform.SetParent(transform);
        borders[1].transform.SetParent(transform);
        borders[8].transform.SetParent(transform);

        borders[0].transform.localScale = new Vector3(borders[0].transform.localScale.x, borders[0].transform.localScale.y, 1.0f);
        borders[1].transform.localScale = new Vector3(1.0f, borders[1].transform.localScale.y, borders[1].transform.localScale.z);
        borders[8].transform.localScale = new Vector3(borders[8].transform.localScale.x, 1.0f, borders[8].transform.localScale.z);

        borders[2].transform.localScale = borders[0].transform.localScale;
        borders[4].transform.localScale = borders[0].transform.localScale;
        borders[6].transform.localScale = borders[0].transform.localScale;

        borders[3].transform.localScale = borders[1].transform.localScale;
        borders[5].transform.localScale = borders[1].transform.localScale;
        borders[7].transform.localScale = borders[1].transform.localScale;

        borders[9].transform.localScale = borders[8].transform.localScale;
        borders[10].transform.localScale = borders[8].transform.localScale;
        borders[11].transform.localScale = borders[8].transform.localScale;

        for (var i = 0; i < borders.Length; i++)
            borders[i].transform.localRotation = Quaternion.identity;
    }
}
