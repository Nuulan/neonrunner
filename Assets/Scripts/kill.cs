﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kill : MonoBehaviour
{
    
    public Animator anim;
    public Animation cut;
    public GameObject[] sabre;
    public bool shootAble = true;

    private void Start()
    {
        anim = GetComponent<Animator>();
        cut = GetComponent<Animation>();
        sabre = GameObject.FindGameObjectsWithTag("Saber");
        sabre[0].SetActive(false);
        Debug.Log("sabre"+ sabre[0]);
        shootAble = true;
        anim.SetBool("Walking", true);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && shootAble == true)
        {
            sabre[0].SetActive(true);
            Debug.Log("sabre true");
            shootAble = false;
            
           
            anim.SetBool("Slashing", true);
            anim.SetBool("Walking", false);
            anim.Play("Slash");
            anim.Play("Blade");
            cut.Play("blade");

            StartCoroutine(ShootingYield());

            //anim.CrossFade("Slash", 0);
     
        }
    }

    IEnumerator ShootingYield()
    {
        yield return new WaitForSeconds(0.9f);
        anim.SetBool("Slashing", false);
        //anim.SetBool("Walking", true);
       
        sabre[0].SetActive(false);
        shootAble = true;
    }



}
