﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{
    public Animator anim;
    public bool shootAble = true;
    public bool isIn = false;
    public Collider target = null;
    //public Animation cut; 
    // Start is called before the first frame update
    void Start()
    {
       // cut = GetComponent<Animation>();
        //anim = GetComponent<Animator>();
        shootAble = true;
        anim.SetBool("Walking", true);
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {

        if (other.transform.tag == "Enemy")
        {
            isIn = true;
            target = other;
            Debug.Log("blade"+ target);
        }
    }

    //// Start is called before the first frame update
    //private void OnTriggerExit(Collider other)
    //{

    //    if (other.transform.tag == "Enemy")
    //    {
    //        isIn = false;
    //        target = null;
    //        Debug.Log("bye bye");
    //    }
    //}
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            shootAble = false;
            anim.SetBool("Slashing", true);
            Debug.Log("sunshine");
            //anim.Play("Slash");
            //cut.Play("Blade");
            // cut.Play("blade");

            ;

            //animati.Play("Charactere.Scene");
            //anim.CrossFade("Slash", 0);

            StartCoroutine(ShootingYield());
           

        }
    }
    IEnumerator ShootingYield()
    {

        yield return new WaitForSeconds(0.2f);
        shootAble = true;
        //anim.SetBool("Walking", true);
        anim.SetBool("Slashing", false);
       
        if (isIn == true)
        {
            isIn = false;
            Debug.Log("bim bam boom");
            if (target)
            {
                GameManager.Instance.onKill();
                Destroy(target.gameObject);
                Debug.Log("nooooooooooooooooooooooooooooooooooo");
                target = null;

            }
        }
    }
}
