﻿using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public Slider Slider { get; private set; }

    public Image Image;

    private void Start()
    {
        Slider = GetComponent<Slider>();        
    }

    public void DisplayEnergy(float energyLevel)
    {
        Slider.value = energyLevel;
    }
}
