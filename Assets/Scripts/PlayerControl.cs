﻿using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public Camera Camera;
    public GameObject Head;
    public GameObject HitMarker;
    public ParticleSystem SpeedLines;
    public Animator animator;

    public float CurrentSpeed { get; private set; }

    public float dashPower;
    public float gravity;
    public float jumpPower;
    public int numberOfJumps, numberOfDashes;
    public float sensibility;
    public float baseSpeedOnGround;
    public float maxSpeedOnGround;
    public float maxSpeedInAir;
    public float speedInAir;
    public float speedMultiplier;
    public float timeBeforeMaxSpeed;
    public float timeBeforeBaseSpeed;

    public float HeadBobbingFrequency = 5;
    public Vector2 HeadBobbingAmplitude = new Vector2(0.1f, 0.1f);
    [Range(0, 1)] public float HeadBobbingSmoothing = 0.1f;

    private float baseFOV;
    private float targetFOV;
    private float cameraZOrigin;
    private CharacterController controller;
    private int currentNbJumps;
    private int currentNbDashes;
    private Vector3 currentVelocity;
    private bool dashing;
    private float elapsedSinceWalking;
    private Vector3 targetVelocity;
    private AttackTrigger attack;
    private Wallrun wallRun;

    // Timers
    Timer dashTimer;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        baseFOV = Camera.fieldOfView;
        targetFOV = baseFOV;
        cameraZOrigin = Camera.transform.localPosition.z;
        controller = gameObject.GetComponent<CharacterController>();
        CurrentSpeed = baseSpeedOnGround;
        dashTimer = new Timer(0.2f);
        dashTimer.Finished = () =>
        {
            dashing = false;
            SpeedLines.gameObject.SetActive(false);
            currentVelocity = currentVelocity.magnitude * transform.forward;
        };
        currentNbDashes = 0;
        currentVelocity = Vector3.zero;
        targetVelocity = Vector3.zero;
        attack = GetComponent<AttackTrigger>();
        wallRun = gameObject.GetComponent<Wallrun>();
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.E))
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
            Destroy(enemy);
        }


        var mouse = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        var movement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (controller.isGrounded && currentVelocity.y < 0)
        {
            currentVelocity.y = 0;
            currentNbJumps = numberOfJumps;
            currentNbDashes = numberOfDashes;
        }

        var speedFOV = Mathf.Clamp(40 * (CurrentSpeed - baseSpeedOnGround) / (maxSpeedOnGround - baseSpeedOnGround), 10, 40);
        targetFOV = baseFOV + speedFOV;
        Camera.fieldOfView = Mathf.Lerp(Camera.fieldOfView, targetFOV, 0.5f);

        if (dashing)
        {
            dashTimer.Update(Time.deltaTime);
            controller.Move(speedMultiplier * dashPower * Camera.transform.forward * Time.deltaTime);
            CurrentSpeed += (maxSpeedOnGround - baseSpeedOnGround) / 2;
            SpeedLines.gameObject.SetActive(true);
            attack.Kill();
            return;
        }

        if (Input.GetButtonDown("Fire3") && currentNbDashes > 0)
        {
            dashing = true;
            GameManager.Instance.Dashing();
            FaceForward();
            dashTimer.Start();
            currentNbDashes--;
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FaceForward();
            attack?.Attack();
            animator?.SetBool("IsSlashing", true);
        } else
        {
            animator?.SetBool("IsSlashing", false);
        }

        Head.transform.Rotate(new Vector3(0, mouse.x, 0) * sensibility, Space.Self);
        Head.transform.localEulerAngles = new Vector3(Head.transform.localEulerAngles.x, Head.transform.localEulerAngles.y, wallRun.CameraTilt());

        float cameraVerticalAngle = Camera.transform.localEulerAngles.x - mouse.y * sensibility;
        if (cameraVerticalAngle > 180 && cameraVerticalAngle < 271) cameraVerticalAngle = 271f;
        else if (cameraVerticalAngle > 89 && cameraVerticalAngle <= 180) cameraVerticalAngle = 89f;
        Camera.transform.localEulerAngles = new Vector3(cameraVerticalAngle, 0, 0);

        FaceForward();

        if (movement.x != 0 || movement.y != 0)
        {
            elapsedSinceWalking += Time.deltaTime;
            animator.SetBool("IsRunning", true);
        }
        else
        {
            elapsedSinceWalking = 0;
            animator.SetBool("IsRunning", false);
        }

        float maxSpeedAccordingState = 0;

        if (controller.isGrounded || wallRun.IsWallRunning)
        {
            if (controller.isGrounded)
            {
                var targetCameraPosition = CalculateHeadBobbingDelta(elapsedSinceWalking);
                targetCameraPosition.z = cameraZOrigin;
                Camera.transform.localPosition = Vector3.Lerp(Camera.transform.localPosition, targetCameraPosition, HeadBobbingSmoothing);
                if (movement.y > 0)
                    CurrentSpeed += (maxSpeedOnGround - baseSpeedOnGround) * Time.deltaTime / timeBeforeMaxSpeed;
                else
                    CurrentSpeed -= (maxSpeedOnGround - baseSpeedOnGround) * Time.deltaTime / timeBeforeBaseSpeed;
                maxSpeedAccordingState = maxSpeedOnGround;
                CurrentSpeed = Mathf.Clamp(CurrentSpeed, baseSpeedOnGround, maxSpeedOnGround);
                targetVelocity = speedMultiplier * CurrentSpeed * (movement.x * transform.right + movement.y * transform.forward);
                currentVelocity = Vector3.Lerp(currentVelocity, targetVelocity, Time.deltaTime * 10.0f);
                currentVelocity.y += gravity * Time.deltaTime;
            } else
            {
                maxSpeedAccordingState = maxSpeedInAir;
                targetVelocity = speedMultiplier * CurrentSpeed * wallRun.WallRunVelocity;
                if (targetVelocity.magnitude > maxSpeedInAir * speedMultiplier)
                    targetVelocity = targetVelocity.normalized * maxSpeedInAir * speedMultiplier;
                currentVelocity = Vector3.Lerp(currentVelocity, targetVelocity, Time.deltaTime * 10.0f);
                currentVelocity.y += gravity * Time.deltaTime;
            }

            if (Input.GetButtonDown("Jump"))
            {
                if (controller.isGrounded)
                {
                    currentVelocity.y = jumpPower;
                    currentNbJumps--;
                }
                else
                {
                    currentVelocity.y = 0;
                    currentVelocity += wallRun.WallJumpDirection() * jumpPower;
                }
            }
        } else
        {
            if (!wallRun.IsWallRunning)
            {
                Vector3 verticalSpeed = Vector3.up * currentVelocity.y;
                Vector3 horizontalSpeed = Vector3.ProjectOnPlane(currentVelocity, transform.up);

                horizontalSpeed += speedMultiplier * speedInAir * Time.deltaTime * (movement.x * transform.right + movement.y * transform.forward);

                if (horizontalSpeed.magnitude > maxSpeedInAir * speedMultiplier)
                    horizontalSpeed = horizontalSpeed.normalized * maxSpeedInAir * speedMultiplier;

                currentVelocity = horizontalSpeed + verticalSpeed;
                maxSpeedAccordingState = maxSpeedInAir;

                if (Input.GetButtonDown("Jump") && currentNbJumps --> 0)
                    currentVelocity.y = jumpPower;
                currentVelocity.y += gravity * Time.deltaTime;
            }
        }

        if (currentVelocity.magnitude > maxSpeedAccordingState * 1.5f)
            SpeedLines.gameObject.SetActive(true);
        else
            SpeedLines.gameObject.SetActive(false);

        controller.Move(currentVelocity * Time.deltaTime);
    }

    Vector3 CalculateHeadBobbingDelta(float t)
    {
        if (t <= 0) return Vector3.zero;
        return Camera.transform.right * Mathf.Cos(t * HeadBobbingFrequency) * HeadBobbingAmplitude
            + Camera.transform.up * Mathf.Sin(t * HeadBobbingFrequency * 2) * HeadBobbingAmplitude;
    }

    void FaceForward()
    {
        transform.rotation = Quaternion.Euler(0, Head.transform.rotation.eulerAngles.y, 0);
        Head.transform.localRotation = Quaternion.Euler(new Vector3(Head.transform.localRotation.eulerAngles.x, 0, Head.transform.localRotation.eulerAngles.z));
    }

    public void AddDash()
    {
        currentNbDashes++;
    }
}
