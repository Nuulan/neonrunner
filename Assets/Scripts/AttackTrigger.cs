﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{
    public GameObject deathParticles;
    public float timeOffset;
    private List<GameObject> enemiesInRange;
    private Coroutine attackRoutine;

    private void Start()
    {
        enemiesInRange = new List<GameObject>();
    }

    public void Attack()
    {
        if (attackRoutine == null)
            attackRoutine = StartCoroutine(AttackRoutine());
    }

    public void Kill()
    {
        foreach (var enemy in enemiesInRange)
        {
            var obj = GameObject.Instantiate(deathParticles);
            obj.transform.SetParent(enemy.transform);
            obj.transform.localPosition = new Vector3(0, 4.0f, 0);
            obj.transform.SetParent(null);
            GameManager.Instance.onKill();
            Destroy(enemy);
        }
        enemiesInRange.Clear();
    }

    IEnumerator AttackRoutine()
    {
        yield return new WaitForSeconds(timeOffset);
        Kill();
        attackRoutine = null;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Enemy"))
            return;
        enemiesInRange.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (enemiesInRange.Contains(other.gameObject))
            enemiesInRange.Remove(other.gameObject);

    }
}
