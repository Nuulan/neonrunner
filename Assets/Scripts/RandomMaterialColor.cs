﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMaterialColor : MonoBehaviour
{
    public Color[] Colors;

    void Awake()
    {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        if (renderer == null) return;
        if (renderer.material == null) return;
        renderer.material.SetColor("_BaseColor", Colors[Random.Range(0, Colors.Length)]);
    }
}
