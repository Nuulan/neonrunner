﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallrun : MonoBehaviour
{
    public float cameraRollAngle;
    public float cameraTransitionDuration;
    public float jumpDuration;
    public float wallBouncing;
    public float wallDistance;
    public float wallSpeed;
    public float orientationThreshold;

    public bool IsWallRunning { get; private set; }
    public Vector3 WallRunVelocity { get; private set; }

    private CharacterController controller;
    private PlayerControl control;
    private Vector3[] directions;
    private Vector3 hitPosition;
    private Vector3 hitNormal;

    private float elapsedSinceJump;
    private float elapsedSinceAttach;
    private float elapsedSinceDetach;

    private bool disabled;
    private int disabledStacks;

    // Start is called before the first frame update
    void Start()
    {
        control = GetComponent<PlayerControl>();
        controller = GetComponent<CharacterController>();
        directions = new Vector3[] {
            -transform.right,
            transform.forward - transform.right,
            transform.forward,
            transform.forward + transform.right,
            transform.right
        };
        hitPosition = Vector3.zero;
        hitNormal = Vector3.zero;
        disabledStacks = 0;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedSinceJump += Time.deltaTime;

        // can wall jump
        if (!CanWallRun() || Input.GetAxisRaw("Vertical") <= 0)
        {
            elapsedSinceDetach += Time.deltaTime;
            elapsedSinceAttach = 0;
            CancelWallRun();
        }
        else
        {
            elapsedSinceAttach += Time.deltaTime;

            RaycastHit closestWall = GetClosestWallInRange();
            if (closestWall.collider != null) // if wall found
            {
                elapsedSinceDetach = 0;
                hitPosition = closestWall.point;
                hitNormal = closestWall.normal;
                IsWallRunning = true;
                WallRunVelocity = wallSpeed * (transform.forward - closestWall.normal * Vector3.Dot(transform.forward, closestWall.normal)).normalized;
                Debug.DrawRay(transform.position, WallRunVelocity.normalized * 2, Color.blue);
            } else
            {
                elapsedSinceDetach += Time.deltaTime;
                elapsedSinceAttach = 0;
                CancelWallRun();
            }
        }

        if (Input.GetButtonDown("Jump"))
            elapsedSinceJump = 0;
    }

    bool CanWallRun() => !controller.isGrounded && elapsedSinceJump > jumpDuration && !disabled;
    void CancelWallRun()
    {
        IsWallRunning = false;
        WallRunVelocity = Vector3.zero;
    }

    RaycastHit GetClosestWallInRange()
    {
        var hits = new List<RaycastHit>();

        for (var i = 0; i < directions.Length; i++)
        {
            Vector3 direction = transform.TransformDirection(directions[i]);
            RaycastHit hit;
            Physics.Raycast(transform.position, direction, out hit, wallDistance);

            if (hit.collider && hit.collider.CompareTag("WallRideable") && Vector3.Dot(hit.normal, Vector3.up) < orientationThreshold)
            {
                hits.Add(hit);
                Debug.DrawRay(transform.position, direction * hit.distance, Color.green);
            } 
            else
                Debug.DrawRay(transform.position, direction * 1, Color.red);
        }

        // if walls found
        if (hits.Count > 0)
        {
            hits.Sort(delegate (RaycastHit h1, RaycastHit h2) {
                if (h1.distance == h2.distance) return 0;
                else if (h1.distance > h2.distance) return 1;
                else return -1;
            });

            return hits[0]; // return the closest one
        }

        return new RaycastHit();
    }


    float CalculateSide() => (IsWallRunning) ? Vector3.Dot(Vector3.Cross(transform.forward, hitPosition - transform.position), transform.up) : 0;

    public float CameraTilt()
    {
        float dir = CalculateSide();
        float targetAngle = 0;
        if (dir != 0) 
            targetAngle = Mathf.Sign(dir) * cameraRollAngle;
        return Mathf.LerpAngle(control.Head.transform.localEulerAngles.z, targetAngle, Mathf.Max(elapsedSinceAttach, elapsedSinceDetach) / cameraTransitionDuration);
    }

    public Vector3 WallJumpDirection() => (IsWallRunning) ? (hitNormal * wallBouncing + transform.up) : Vector3.zero;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WallRunLimit"))
        {
            disabled = true;
            disabledStacks++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("WallRunLimit"))
            disabledStacks--;

        if (disabledStacks <= 0)
            disabled = false;
    }
}
