﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    public float Time { get; private set; }
    public bool Paused { get; private set; }
    public Action Finished;

    private float current;

    public Timer(float time)
    {
        Time = time;
    }

    public void Start()
    {
        current = 0;
        Paused = false;
    }

    public void Resume()
    {
        Paused = false;
    }

    public void Pause()
    {
        Paused = true;
    }

    public void Update(float delta)
    {
        current += delta;
        if (current >= Time)
            Finished?.Invoke();
    }

    public bool IsFinished() => current >= Time;
}
