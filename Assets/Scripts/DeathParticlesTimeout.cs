﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathParticlesTimeout : MonoBehaviour
{
    public float timeout;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Timeout());
    }

    IEnumerator Timeout()
    {
        yield return new WaitForSeconds(timeout);
        Destroy(gameObject);
    }
}
