﻿using System.Collections;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform[] Barrels;
    public GameObject Bullet;
    public PlayerControl Player;
    public Animator animator;
    public float BulletSpeed;
    public float DespawnTime = 3.0f;
    public float LoadingTime;
    public float Range;
    public float WaitBeforeNextShot;

    private int currentBarrel;
    private Coroutine shootingRoutine;

    private void Start()
    {
        currentBarrel = 0;
    }

    void Update()
    {
        if (Player == null)
            return;

        Vector3 E2T = (Player.transform.position - transform.position); // Enemy To Target

        if (E2T.magnitude < Range) {
            animator.SetBool("IsShooting", true);
            transform.rotation = Quaternion.LookRotation(E2T, Vector3.up);
            if (shootingRoutine == null)
                shootingRoutine = StartCoroutine(ShootingYield());
        } else
        {
            animator.SetBool("IsShooting", false);
            if (shootingRoutine != null)
            {
                StopCoroutine(shootingRoutine);
                shootingRoutine = null;
            }
        }
    }


    IEnumerator ShootingYield()
    {
        yield return new WaitForSeconds(LoadingTime);
        while (true)
        {
            Shoot();
            yield return new WaitForSeconds(WaitBeforeNextShot);
        }
    }

    void Shoot()
    {
        var barrel = Barrels[currentBarrel++];
        if (currentBarrel == Barrels.Length)
            currentBarrel = 0;
        var bullet = Instantiate(Bullet);
        bullet.transform.position = barrel.position;
        bullet.transform.rotation = barrel.rotation;
        var projectile = bullet.GetComponent<EnemyProjectile>();
        projectile.Velocity = (Player.HitMarker.transform.position - bullet.transform.position).normalized * BulletSpeed;
        projectile.Lifetime = DespawnTime;
    }
}
